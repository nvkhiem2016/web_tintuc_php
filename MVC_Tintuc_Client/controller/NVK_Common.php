<?php 
	function load()
	{
		$init=include('config/init.php');
		require_once('NVK_Controller.php');
		//c=controller,a=action
		$controller = empty($_GET['c'])?$init['default_controller'] : $_GET['c'];//Get gia tri controller (c)
		$controller = 'c_'.$controller;
		$action = empty($_GET['a'])?$init['default_action']: $_GET['a']; 
		if(!file_exists('controller/'.$controller.'.php'))
			die('Controller không tồn tại');
		require_once('controller/'.$controller.'.php');
		$control = new $controller();
		if(!method_exists($controller,$action))
			die('Action không tồn tại');
		$control -> $action();
	}
	load();
?>