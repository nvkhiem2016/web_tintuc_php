<?php 
    
	class c_user extends NVK_Controller
	{
        public $user;
		function __construct()
		{
			parent::__construct();
			$this-> load->model('m_user','');
			//load library
			$this-> load->library('');
			//load helper
			$this-> load->helper('');
			$this-> load->helper('');
			//load model

			$this-> user = new m_user();
		}
        
        public function html($data1,$path,$data2,$data3)
		{
			$this-> load-> view('header',$data1);
			$this-> load-> view('user/'.$path,$data2);
			$this-> load-> view('footer',$data3);
        }
        public function index()
		{
			
			$path ='index';
            $this->html($data,$path,'','');
			
        }
        public function getDangky(){
            $data['title'] = "Trang Đăng Ký";
            $path = 'dangky';
            $this->html($data['title'],$path,'','');
        }
        public function postDangky(){
           if(isset($_POST['btnDangky'])){
                $data['title'] = "Trang đăng ký";
                $data['name'] = $_POST['name'];
                $data['email'] = $_POST['email'];
                $data['password'] = md5($_POST['password']);
                $data['passwordAgain'] = md5($_POST['passwordAgain']);
                if($data['passwordAgain']===$data['password']){
                    
                    $data['user'] = $this->user->getUserbyEmail($data['email']);
                    $num = $this->user->Num_Rows();
                    if($num>0){
                        $data['message'] = "Email đã tồn tại,vui lòng đặt email khác";
                        return $this->html($data,'dangky',$data ,'');
                    }else{
                        $insert = $this->user->insertUser($data);
                        if($insert){
                            $data['message'] = "Đăng ký tài khoản thành công";
                            return $this->html($data,'dangky',$data ,'');
                        }
                    }
                }else{
                    $data['message'] = "Mật khẩu nhập lại không khớp";
                    return $this->html($data,'dangky',$data ,'');
                }
                
           }else{
              header('location:index.php');
           }
        }
        public function getDangnhap()
        {
            $data['title'] = "Trang Đăng Nhập";
            $path = 'dangnhap';
            $this->html($data,$path,'','');
        }
        public function postDangnhap()
        {
            if(isset($_POST['btnDangnhap'])){
                $email = isset($_POST['email'])?$_POST['email']:'';
                $password =isset($_POST['password'])?$_POST['password']:'';
                $password = md5($password);//mã hoá mật khẩu
                $data['data'] = $this->user->loginUser($email,$password);
                if($data['data']){
                    //$id=$data['data']['id'];
                    setcookie('user',$email,time() + (86400 * 30));             
                    session_start();
                    $_SESSION['user_name'] = $data['data']['name'];
                    $_SESSION['user_id'] = $data['data']['id'];
                    unset($_SESSION['chua_dang_nhap']);
                    header('location:index.php');
                }else{
                    $data['message'] = "Tài khoản không tồn tại, hoặc mật khẩu không chính xác";
                    //echo "<script type='text/javascript'>alert('{$data['message']}');</script>";
                    return $this->html('','dangnhap',$data ,'');
                }
            }
        } 
        public function dangxuat()
        {
            $email =  $_COOKIE['admin'];
            session_start();
            session_destroy();
            setcookie('admin',$email,time()-1);//set time về 0
            header('location:index.php');
        }
	}
?>