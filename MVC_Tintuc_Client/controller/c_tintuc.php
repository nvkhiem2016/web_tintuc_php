<?php 
	session_start();
	class c_tintuc extends NVK_Controller
	{
		public $tintuc;
		function __construct()
		{
			parent::__construct();
			//load model
			$this->load->model('m_tintuc','');
			//load helper
			$this-> load->helper('');
			//load library
			$this-> load->library('pagination');
			// khởi tạo đối tượng
			$this->tintuc = new m_tintuc();
		}
		public function html($data1,$path,$data2,$data3)
		{
			$this-> load-> view('header',$data1);
			$this-> load-> view('tintuc/'.$path,$data2);
			$this-> load-> view('footer',$data3);
		}
		public function index()
		{
			$data['title'] = "Trang tin tức";
			$path = 'index';
			$data['menu'] = $this->tintuc->getMenu();
			$data['slide'] = $this->tintuc->getSlide();
			
			return $this->html($data,$path,$data,'');
		}

		public function getAllTintucByIdLoaitin()// lấy tin tức theo id loại tin
		{
			$path='loaitin';
			$data['title'] = "Loại tin";
			$id_loaitin = (isset($_GET['id_loai']))?$_GET['id_loai']:1;
			$data['tenloaitin'] = $this->tintuc->getTenloaitinById($id_loaitin);
			$data['danhmuctin'] = $this->tintuc->getAllTintucByIdLoai($id_loaitin);
			$data['menu'] = $this->tintuc->getMenu();
			$trang_hientai = (isset($_GET['page']))?$_GET['page']:1;
			
			$pagination = new pagination(count($data['danhmuctin']),$trang_hientai,1,2);//phân trang
			$data['thanhphantrang'] = $pagination->showPagination();//show thanh phân trang
			$limit = $pagination->_nItemOnPage;
			
			$vitri = ($trang_hientai)*$limit;
			$data['danhmuctin'] = $this->tintuc->getAllTintucByIdLoai($id_loaitin,$vitri,$limit);
			$data['ten'] = $this->tintuc->getTitlebyId($id_loaitin);
			
			return $this->html($data,$path,$data,'');
		}
		public function getTintuc() // xem chi tiết 1 tin
		{
			$id_tin = isset($_GET['id_tin'])?$_GET['id_tin']:'';
			$loai_tin = isset($_GET['loai_tin'])?$_GET['loai_tin']:'';//ten không dấu của loại tin
			$data['title'] = 'Tin tức';
			$data['chitietTin'] = $this->tintuc->getChitietTin($id_tin);//lấy chi tiết của tintuc
			$data['comment'] = $this->tintuc->getComment($id_tin);//lấy comment của tintuc
			$data['relatednews'] = $this->tintuc->getRelatedNews($loai_tin);
			$data['tinnoibat'] = $this->tintuc->getTinNoiBat();
			$path = 'chitiet';
			return $this->html($data,$path,$data,'');
		}
		public function searchTintuc()
		{
			if(isset($_POST['btnSearch']))
			{
				$key = $_POST['tukhoa'];
				$data['title'] = "Tìm kiếm tin tức";
				$path = 'timkiem';
				$data['data'] = $this->tintuc->searchTintuc($key);
				$data['menu'] = $this->tintuc->getMenu();
				return $this->html($data,$path,$data,'');
			}
		}
		function addComment(){
			if(isset($_POST['btnBinhluan'])){
				if(isset($_SESSION['user_id'])){
					$idUser = $_SESSION['user_id'];// khi đăng nhập sẽ có usêr_id
					$idTin = $_POST['id_tin'];
       				$noidung = $_POST['noidung'];
       				$data['addComment'] = $this->tintuc->addComment($idUser,$idTin,$noidung);
					header("location:".$_SERVER['HTTP_REFERER']); //quay lại trang trước đó
				}else{
					$_SESSION['chua_dang_nhap']="Vui lòng đăng nhập để bình luận";
					header("location:".$_SERVER['HTTP_REFERER']);
				}
			}
		}
	}
?>