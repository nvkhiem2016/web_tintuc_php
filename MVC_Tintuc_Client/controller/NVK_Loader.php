<?php 

/**
* 
*/
class NVK_Loader
{
	
	function __construct()
	{
		//$this-> autoload();
	}
	public function view($file,$data)
	{
		//echo './view/'.'aa'.'.php';die();

		if(isset($file) && !empty($file))
		{

			if(file_exists('views/'.$file.'.php'))
			{
				require_once('views/'.$file.'.php');
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	public function model($file,$data)
	{
		if(isset($file) && !empty($file))
		{
			//$file = 'm_'.$file;
			if(file_exists('models/'.$file.'.php'))
			{
				require_once('models/'.$file.'.php');
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	public function library($file)
	{
			$file = $file.'_library';
			if(file_exists('library/'.$file.'.php'))
			{
				require_once('library/'.$file.'.php');
			}
			else
			{
				return false;
			}
	}
	public function helper($file)
	{

		$file = $file.'_helper';
		if(file_exists('helper/'.$file.'.php'))
		{
			require_once('helper/'.$file.'.php');
		}
		else
		{
			return false;
		}
		
	}
	public function autoload()
	{
		require_once('config/autoload.php');
		foreach ($auto_library as $key => $value)
		{
			$this-> library($value);
		}
		foreach ($auto_helper as $key => $value) 
		{
			$this-> helper($value);
		}
	}
}
?>