<?php 


$tintuc = $data['danhmuctin'];

$menu = $data['menu'];
$title = $data['ten'];
$thanhphantrang = $data['thanhphantrang'];

$TenKhongDau = $data['tenloaitin']['TenKhongDau'];
?>




    <!-- Page Content -->
    <div class="container">


        <div class="space20"></div>


       <div class="row main-left">
            <div class="col-md-3 ">
                <ul class="list-group" id="menu">
                    <li href="#" class="list-group-item menu1 active">
                    	Menu
                    </li>
                    <?php 
                    foreach ($menu as $mn) 
                    {
                    ?>
                     <li href="#" class="list-group-item menu1">
                        <?=$mn['Ten']?>
                    </li>
                    <ul>
                    <?php
                    $loaitin = explode(',',$mn['LoaiTin']);//phân cách mảng khi có ','
                    foreach ($loaitin as $loai) 
                    {
                    list($id,$ten,$tenkhongdau) = explode(':',$loai);
                    ?>
                        <li class="list-group-item">
                            <a href="?c=tintuc&a=getAllTintucByIdLoaitin&id_loai=<?=$id?>"><?=$ten?></a>
                        </li>
                    <?php
                    }
                    ?>  
                    </ul>
                    <?php     
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-9 " id="dataSearch">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:#337AB7; color:white;">
                        <h4><b><?=$title['Ten']?></b></h4>
                    </div>
                    <?php 
                    foreach($tintuc as $tin){
                        
                    ?>
                    
                    <div class="row-item row">
                        <div class="col-md-3">
                            <a href="?c=tintuc&a=getTintuc&loai_tin=<?=$TenKhongDau?>&id_tin=<?=$tin['id']?>">
                                <br>
                                <img width="200px" height="200px" class="img-responsive" src="../tintuc/<?=$tin['Hinh']   ?>" alt="">
                            </a>
                        </div>

                        <div class="col-md-9">
                            <h3><?=$tin['TieuDe']?></h3>
                            <p><?=$tin['TomTat']?></p>
                            <a class="btn btn-primary" href="?c=tintuc&a=getTintuc&loai_tin=<?=$TenKhongDau?>&id_tin=<?=$tin['id']?>">Đọc thêm<span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                        <div class="break"></div>
                    </div>
                    <?php    
                    }
                    ?>
                    
                </div>

                    <!-- Pagination -->
                    <div class="row text-center">
                        <div class="col-lg-12">
                            <ul class="pagination">
                                <?=$thanhphantrang //phân trang?>
                            </ul>
                        </div>
                    </div>
                    <!-- /.row -->

                </div>
                </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- end Page Content -->

    <!-- Footer -->
    <hr>
