<?php 



$slide = $data['slide'];
$menu = $data['menu'];


?>
    <!-- Page Content -->
    <div class="container">

    	<!-- slider -->
    	<div class="row carousel-holder">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                    <?php 
                        for($i=0;$i<count($slide);$i++)
                        {
                            if($i==0)
                            {
                    ?>
                            <div class="item active">
                                <img class="slide-image" src="public/image/slide/<?=$slide[$i]['Hinh']?>" alt="">
                            </div>
                    <?php
                            }
                            else
                            {
                    ?>
                            <div class="item">
                                <img class="slide-image" src="public/image/slide/<?=$slide[$i]['Hinh']?>" alt="">
                            </div>
                            <div class="item">
                                <img class="slide-image" src="public/image/slide/<?=$slide[$i]['Hinh']?>" alt="">
                            </div>
                    <?php   
                            }
                        }
                    ?>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
        <!-- end slide -->

        <div class="space20"></div>


        <div class="row main-left">
            <div class="col-md-3 ">
                <ul class="list-group" id="menu">
                    <li href="#" class="list-group-item menu1 active">
                    	Menu
                    </li>
                    <?php 
                    foreach ($menu as $mn) 
                    {
                    ?>
                     <li href="#" class="list-group-item menu1">
                        <?=$mn['Ten']?>
                    </li>
                    <ul>
                    <?php
                    $loaitin = explode(',',$mn['LoaiTin']);//phân cách mảng khi có ','
                    
                    foreach ($loaitin as $loai) 
                    {
                    list($id,$ten,$tenkhongdau) = explode(':',$loai);

                    ?>
                        <li class="list-group-item">
                            <a href="?c=tintuc&a=getAllTintucByIdLoaitin&id_loai=<?=$id?>"><?=$ten?></a>
                        </li>
                    <?php
                    }
                    ?>  
                    </ul>
                    <?php     
                    }
                    ?>
                </ul>
            </div>

            <div class="col-md-9" id="dataSearch">
	            <div class="panel panel-default">
	            	<div class="panel-heading" style="background-color:#337AB7; color:white;" >
	            		<h2 style="margin-top:0px; margin-bottom:0px;"> Tin Tức</h2>
	            	</div>

	            	<div class="panel-body">
	            		<!-- item -->
						<?php 

						foreach($menu as $mn) {
						?>
						<div class="row-item row">
		                	<h3>
		                		<a href="#"><?=$mn['Ten']?></a> |
								<?php
                                 $loaitin = explode(',',$mn['LoaiTin']);//phân cách mảng khi có ','
                                
								 foreach($loaitin as $loai){
									list($id,$ten,$tenkhongdau) = explode(':',$loai);
									?>
									<small><a href="?c=tintuc&a=getAllTintucByIdLoaitin&id_loai=<?=$id?>"><i><?=$ten?></i></a>/</small>
									<?php
								 }
								?>
		                	</h3>
		                	<div class="col-md-12 border-right">
		                		<div class="col-md-3">
			                        <a href="?c=tintuc&a=getTintuc&loai_tin=<?=$tenkhongdau?>&id_tin=<?=$mn['id_tin']?>">
			                            <img class="img-responsive" src="../tintuc/<?=$mn['HinhTin']?>" alt="">
			                        </a>
			                    </div>

			                    <div class="col-md-9">
			                        <h3><?=$mn['TieuDeTin']?></h3>
			                        <p><?=$mn['TomTat']?></p>
			                        <a class="btn btn-primary" href="?c=tintuc&a=getTintuc&loai_tin=<?=$mn['TenKhongDau']?>&id_tin=<?=$mn['id_tin']?>">Đọc thêm <span class="glyphicon glyphicon-chevron-right"></span></a>
								</div>

		                	</div>

							<div class="break"></div>
		                </div>
						<?php	
						}
						?>	    
		                <!-- end item -->
					</div>
	            </div>
        	</div>
        </div>
        <!-- /.row -->
    </div>
    <!-- end Page Content -->

    <!-- Footer -->
    <hr>
