<?php 


$tintuc = $data['chitietTin'];//chi tiết của tin tức
$comment = $data['comment'];//comment của tin tức
$relatednews = $data['relatednews'];//tin liên quan
$tinnoibat = $data['tinnoibat'];//lấy row tin tức


//print_r($tintuc);
//sprint_r($relatednews);



?>



    

    <!-- Page Content -->
    <div class="container">
    <div class="row">

<!-- Blog Post Content Column -->
<div class="col-lg-9">

    <!-- Blog Post -->

    <!-- Title -->
    <h1><?=$tintuc['TieuDe']?></h1>

    <!-- Author -->
    <p class="lead">
        by <a href="#">Admin</a>
    </p>

    <!-- Preview Image -->
    <img class="img-responsive" src="../tintuc/<?=$tintuc['Hinh']?>" alt="">

    <!-- Date/Time -->
    <p><span class="glyphicon glyphicon-time"></span> <?=$tintuc['created_at']?></p>
    <hr>

    <!-- Post Content -->
    <p class="lead"><?=$tintuc['NoiDung']?></p>

    <hr>

    <!-- Blog Comments -->

    <!-- Comments Form -->
    <?php 
    if(isset($_SESSION['chua_dang_nhap'])){
        echo "<div class='alert alert-danger'>".$_SESSION['chua_dang_nhap']."</div>";
    }
    ?>
    <div class="well">
        <h4>Viết bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
        <form method="POST" action="?c=tintuc&a=addComment">
            <input type="hidden" name="id_tin" value="<?=$tintuc['id']?>" />
            <div class="form-group">
                <textarea class="form-control" name="noidung" rows="3"></textarea>
            </div>
            <input type="submit" class="btn btn-primary" name="btnBinhluan" value="Gửi"></input>
        </form>
    </div>

    <hr>

    <!-- Posted Comments -->

    <!-- Comment -->
    <?php 
    foreach($comment as $cmt){
    ?>
   <div class="media">
        <a class="pull-left" href="#">
            <img class="media-object" src="http://placehold.it/64x64" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">
                <small><?=$cmt['created_at']?></small>
            </h4>
            <p ><b class="cmt"><?=$cmt['name']." : "?></b><?=$cmt['NoiDung']?></p>
            
        </div>
    </div>
    <?php
    }
    ?>
    

</div>

<!-- Blog Sidebar Widgets Column -->
<div class="col-md-3">

    <div class="panel panel-default">
        <div class="panel-heading"><b>Tin liên quan</b></div>
        <div class="panel-body">
        <?php 
        foreach($relatednews as $related){
        ?>
            <!-- item -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-5">
                    <a href="?c=tintuc&a=getTIntuc&loai_tin=<?=$related['TenKhongDau']?>&id_tin=<?=$related['id'
                    ]?>">
                        <img class="img-responsive" src="../tintuc/<?=$related['Hinh']?>" alt="">
                    </a>
                </div>
                <div class="col-md-7">
                    <a href="?c=tintuc&a=getTIntuc&loai_tin=<?=$related['TenKhongDau']?>&id_tin=<?=$related['id']?>"><b><?=$related['TieuDe']?></b></a>
                </div>
                <p><?=$related['TomTat']?></p>
                <div class="break"></div>
            </div>
            <!-- end item -->
        <?php
        }
        ?>
    
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><b>Tin nổi bật</b></div>
        <div class="panel-body">
        <?php 
        foreach($tinnoibat as $tin){
        ?>
            <!-- item -->
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-5">
                    <a href="?c=tintuc&a=getTIntuc&loai_tin=<?=$tin['TenKhongDau']?>&id_tin=<?=$tin['id']?>">
                        <img class="img-responsive" src="../tintuc/<?=$tin['Hinh']?>" alt="">
                    </a>
                </div>
                <div class="col-md-7">
                    <a href="?c=tintuc&a=getTIntuc&loai_tin=<?=$tin['TenKhongDau']?>&id_tin=<?=$tin['id']?>"><b><?=$tin['TieuDe']?></b></a>
                </div>
                <p><?=$tin['TomTat']?></p>
                <div class="break"></div>
            </div>
            <!-- end item -->
        <?php
        }
        ?>
        </div>
    </div>
    
</div>

</div>


       
    </div>
    <!-- end Page Content -->

    <!-- Footer -->
    <hr>
