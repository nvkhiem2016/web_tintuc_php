<?php 
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    }
     
?>
<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title><?=isset($data['title'])?$data['title']:"Tin Tức"?></title>
    
    <!-- Bootstrap Core CSS -->
    <script
  		src="https://code.jquery.com/jquery-3.3.1.js"
  		integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  		crossorigin="anonymous"></script>
	<script src="public/js/Xuly.js"></script>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/footer.css">

    <!-- Custom CSS -->
    <link href="public/css/shop-homepage.css" rel="stylesheet">
    <link href="public/css/my.css" rel="stylesheet">


</head>
 <!-- Navigation -->
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Tin Tức</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="gioithieu.php">Giới thiệu</a>
                    </li>
                    <li>
                        <a href="lienhe.php">Liên hệ</a>
                    </li>
                </ul>
                
                <form class="navbar-form navbar-left" method="POST" action="index.php?c=tintuc&a=searchTintuc">
			        <div class="form-group">
			          <input type="text" class="form-control" id="tukhoa" name="tukhoa" placeholder="Search">
			        </div>
			        <input type="submit" class="btn btn-default"id="btnSearch" name="btnSearch" value="Search"></input>
			    </form>

			    <ul class="nav navbar-nav pull-right">
                    
                    <?php 
                    if(isset($_SESSION['user_name']))
                    {
                    ?>
                    <li>
                    	<a>
                    		<span class ="glyphicon glyphicon-user"></span>
                    		<?=$_SESSION['user_name']?>
                    	</a>
                    </li>
                    <li>
                    	<a href="index.php?c=user&a=dangxuat">Đăng xuất</a>
                    </li>
                    <?php
                    }else {
                    ?>
                    <li>
                        <a href="index.php?c=user&a=getDangky">Đăng ký</a>
                    </li>
                    <li>
                        <a href="index.php?c=user&a=getDangnhap">Đăng nhập</a>
                    </li>
                    <?php }?>
                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<body>