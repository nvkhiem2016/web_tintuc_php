<?php 

	/**
	* 
	*/
	require_once('NVK_Model.php');
	class m_tintuc extends NVK_Model
	{
		
		function __construct()
		{
			$this->connect();
		}
		public function getMenu(){ // lấy menu
            $sql = "SELECT tl.*,GROUP_CONCAT(Distinct lt.id,':',lt.Ten,':',lt.TenKhongDau) AS LoaiTin,tt.id as idTin,
                tt.TieuDe as TieuDeTin,tt.id as id_tin, tt.Hinh as HinhTin, tt.TomTat as TomTat,tt.TieuDeKhongDau as TieuDeKhongDauTin 
                FROM theloai tl INNER JOIN loaitin lt ON lt.idTheLoai = tl.id INNER JOIN tintuc tt
                ON tt.idLoaiTin = lt.id
                GROUP BY tl.id";
            $this->SetQuery($sql);
            $data = $this->GetAllRows();
			return $data;
        }
        public function getSlide()
        {
            $sql ="SELECT * FROM `slide`";
            $this->SetQuery($sql);
            $data = $this->GetAllRows();
			return $data;
        }
        public function getTenloaitinById($id_loaitin)
        {
            $sql = "SELECT TenKhongDau FROM loaitin WHERE id=$id_loaitin";
            $this->SetQuery($sql);
			return $this->GetRow();
        }
        public function getAllTintucByIdLoai($id_loaitin,$vitri = -1,$limit = -1)//lấy all tin tức = idloaitin
        {
            
            $sql = "SELECT * FROM tintuc WHERE idLoaiTin = $id_loaitin";
            if($vitri>-1 && $limit>1){
                $sql .= " limit $vitri,$limit";
            }
            //echo $sql;
            $this->SetQuery($sql);
            $data = $this->GetAllRows();
            //print_r($data);
            return $data;
        }
        function getTitlebyId($id_loaitin){
            $sql = "SELECT Ten FROM loaitin WHERE id = $id_loaitin";
            $this->SetQuery($sql);
			return $this->GetRow();
        }
        public function getRelatedNews($loaitin)// lấy những tin liên quan đến tin tức đang xem = ten không dấu
        {
            $sql = "select tt.*,lt.TenKhongDau as TenKhongDau 
		    from tintuc tt inner join loaitin lt on lt.id = tt.idLoaiTin
		     where lt.TenKhongDau = '$loaitin' limit 0,2
            ";
            $this->SetQuery($sql);
            $data = $this->GetAllRows();
			return $data;
        }
        public function getComment($id_tin)//lấy comment của tin tức có id_tin
        {
            $sql = "SELECT *,users.name FROM comment,users WHERE idTinTuc=$id_tin and comment.idUser=users.id" ;
            $this->SetQuery($sql);
        
            $data = $this->GetAllRows();
			return $data;
        }
        function getChitietTin($id_tin)//lấy dữ liệu của tin tức
        {
            $sql = "SELECT * FROM tintuc WHERE id=$id_tin";
            $this->SetQuery($sql);
			return $this->GetRow();
        }
        function getTinNoiBat()
        {
            $sql = "select tt.*,lt.TenKhongDau as TenKhongDau 
		    from tintuc tt inner join loaitin lt on lt.id = tt.idLoaiTin
		    where tt.NoiBat = 1 limit 0,3
            ";
            $this->SetQuery($sql);
            $data = $this->GetAllRows();
            return $data;
        }
        public function searchTintuc($key){
            $sql = "select tt.*,lt.TenKhongDau as TenKhongDau from tintuc tt inner join loaitin lt on lt.id = tt.idLoaiTin where TieuDe like '%$key%' or TomTat like '%$key%'";
            $this->SetQuery($sql);
            $data = $this->GetAllRows();
            return $data;
        }
        function addComment($idUser,$idTinTuc,$NoiDung){//thêm bình luận vào bài viết
            $sql = "INSERT INTO comment(idUser,idTinTuc,NoiDung) values($idUser,$idTinTuc,'{$NoiDung}')";
            $data = $this->SetQuery($sql);
            return true;
        }
    }   
?>


