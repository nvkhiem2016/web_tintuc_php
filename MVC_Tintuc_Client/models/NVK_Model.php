<?php
	$database = include('config/database.php');

	class NVK_Model
	{
		private $_host='localhost';
		private $_user='root';
		private $_pass='';
		private $_dbname='tin_tuc';
		private $_conn;
		private $_query;
		function __construct()
		{

		}
		public function connect()
		{
			$this-> _conn = mysqli_connect($this-> _host,$this-> _user,$this-> _pass,$this-> _dbname);
			mysqli_set_charset($this-> _conn,"utf8");

		}
		public function SetQuery($sql)
		{
			$this-> _query = mysqli_query($this-> _conn,$sql);
		}
		public function Num_Rows()
		{
			if($this-> _query)
			{
				return mysqli_num_rows($this-> _query);
			}
			else
				return 0;
		}
		public function GetRow()
		{
			if($this-> _query)
			{
				$data = mysqli_fetch_assoc($this-> _query);
			}
			return $data;
		}
		public function GetAllRows()
		{
			$data = array();
			if($this-> _query)
			{
				while($record = mysqli_fetch_assoc($this-> _query))
				{
					$data[]= $record;
				}
			}
		return $data;
		}
	}
?>