<?php 
	/**
	* 
	*/
	require_once('NVK_Model.php');
	class m_loaitin extends NVK_Model
	{
		function __construct()
		{
			$this->connect();
		}
		public function getAllTheloai()
		{
			$sql = "SELECT id,Ten from theloai where 1";
			$this->SetQuery($sql);
			$data = $this->GetAllRows();
			return $data;
		}
		public function insertLoaitin($data)// insert tin tức mới
		{
			
			$sql = "INSERT INTO loaitin(`idTheLoai`,`TenKhongDau`,`Ten`) values
			('{$data['idTheLoai']}','{$data['TenKhongDau']}','{$data['Ten']}')";
			$this-> SetQuery($sql);
			return true;
		}
		public function updateLoaitin($data)// insert tin tức mới
		{
			$sql = "UPDATE loaitin SET `Ten` ='{$data['Ten']}',
				`TenKhongDau`='{$data['TenKhongDau']}',
				`idTheLoai`='{$data['idTheLoai']}'
			 	WHERE `id` = '{$data['id']}'
			 ";
			$this-> SetQuery($sql);
			return true;
		}
		public function getLoaitinByid($id)
        {
            $sql = "SELECT *FROM loaitin WHERE id=$id";
            $this->SetQuery($sql);
            $data = $this->GetRow();
            return $data;
        }
		public function getAllLoaitin($vitri=-1,$limit=-1)
		{
			$sql = "SELECT loaitin.id,loaitin.TenKhongDau,loaitin.Ten,idTheLoai,theloai.Ten as TenTheLoai FROM loaitin,theloai WHERE 
					loaitin.idTheLoai=theloai.id";
			if($vitri>-1 && $limit>1){
				$sql .= " limit $vitri,$limit";
			}
			$this->SetQuery($sql);
            $data = $this->GetAllRows();
			return $data;
		}
		public function deleteTintuc($id)
		{
			$sql = "DELETE FROM `tintuc` WHERE idLoaiTin = '$id' ";
			$this-> SetQuery($sql);
            return true;
		}
		public function deleteLoaitin($id)//xoá tin tuc thuoc loai tin truoc để xoá loaitin nếu không sẽ bị lỗi khoá ngoại
		{
			$this->deleteTintuc($id);
            $sql = "DELETE FROM `loaitin` WHERE id = '$id'";
            $this-> SetQuery($sql);
			return true;
		}
	}
?>