<?php 
	/**
	* 
	*/
	require_once('NVK_Model.php');
	class m_tintuc extends NVK_Model
	{
		
		function __construct()
		{
			$this->connect();
		}
		public function getTintucbyId($id)//lấy tin tức theo id
		{
			$sql = "SELECT * FROM tintuc where id=$id";
			$this->SetQuery($sql);
			$data = $this->GetRow();
			return $data;
		}
		public function getAllTintucbyLoaitin($id,$vitri=-1,$limit=-1)//lấy all tin tức theo idLoaiTin
		{
			$sql = "SELECT tintuc.id,tintuc.TieuDe,tintuc.TomTat,tintuc.TieuDeKhongDau,tintuc.NoiBat
			 FROM tintuc,loaitin where loaitin.id=$id and loaitin.id = tintuc.idLoaiTin ";
			if($vitri>-1 && $limit>1){
				$sql .= " limit $vitri,$limit";
			}

			$this->SetQuery($sql);
            $data = $this->GetAllRows();
			return $data;
		}
		public function insertTintuc($data)// insert tin tức mới
		{
			
			$sql = "INSERT INTO tintuc(`TieuDe`,`TieuDeKhongDau`,`TomTat`,`NoiDung`,`Hinh`,`NoiBat`,`idLoaiTin`) values
			('{$data['TieuDe']}','{$data['TieuDeKhongDau']}','{$data['TomTat']}','{$data['NoiDung']}','{$data['Hinh']}','{$data['NoiBat']}','{$data['idLoaiTin']}')";
			$this-> SetQuery($sql);
			return true;
		}
		public function updateTintuc($data)// insert tin tức mới
		{
			$sql = "UPDATE tintuc SET `TieuDe` ='{$data['TieuDe']}',
				`TieuDeKhongDau` = '{$data['TieuDeKhongDau']}',
				`TomTat`='{$data['TomTat']}',
				`NoiDung`='{$data['NoiDung']}',
				`Hinh`='{$data['Hinh']}',
				`NoiBat`='{$data['NoiBat']}',
				`idLoaiTin`='{$data['idLoaiTin']}'
			 	WHERE `id` = '{$data['id']}'
			 ";
			
			$this-> SetQuery($sql);
			return true;
		}
		public function getAllTintuc($vitri=-1,$limit=-1)//lấy tất cả tin tức
		{
			$sql = "SELECT * FROM tintuc WHERE 1";
			if($vitri>-1 && $limit>1){
				$sql .= " limit $vitri,$limit";
			}
			$this->SetQuery($sql);
            $data = $this->GetAllRows();
			return $data;
        }
        public function deleteComment($id){//xoá comment
            $sql = "DELETE FROM `comment` WHERE idTinTuc = '$id'";
            $this-> SetQuery($sql);
            return true;
        }
        public function deleteTintuc($id)//xoá comment để xoá tin tức nếu không sẽ bị lỗi khoá ngoại
		{
            $this-> deleteComment($id);
            $sql = "DELETE FROM `tintuc` WHERE id = '$id'";
            $this-> SetQuery($sql);
			return true;
		}
		public function searchTintuc($key,$vitri=-1,$limit=-1)
		{
			$sql = "select tt.*,lt.TenKhongDau as TenKhongDau from tintuc tt inner join loaitin lt on lt.id = tt.idLoaiTin where TieuDe like '%$key%' or TomTat like '%$key%'";
			if($vitri>-1 && $limit>1){
				$sql .= " limit $vitri,$limit";
			}
			$this->SetQuery($sql);
            $data = $this->GetAllRows();
            return $data;
		}
}
?>