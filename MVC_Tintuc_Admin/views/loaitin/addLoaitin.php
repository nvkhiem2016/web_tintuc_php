
<div class="row carousel-holder">
    		<div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default">               
				  	<div class="panel-heading">Thêm loại tin</div>
				  	<div class="panel-body">
                        <form method="POST" action="?c=loaitin&a=postAddLoaitin"  >
                            <div class="form-group">
                                <input type="text" class="form-control" name="ten"  placeholder="Tên loại tin">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="tenkhongdau" placeholder="Tiêu đề không dấu">
                            </div>
                            <div class="form-group">
                                
                                <select name="theloai" id="input" class="form-control">
                                    <option value="-1">-- Select One --</option>
                                    <?php 
                                    foreach($data['theloai'] as $theloai){
                    
                                    ?>
                                    <option value=<?=$theloai['id']?>><?=$theloai['Ten']?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                                
                            </div>
                            <input type="submit" name="btnAddLoaitin" class="btn btn-primary" value="Thêm"></input>
                        </form>  
				  	</div>
				</div>
            </div>
            <div class="col-md-3"></div>
</div>