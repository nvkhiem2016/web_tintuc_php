
 <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
   
   <!--
       instagram: www.instagram.com/programmingtutorial
       site: programlamadersleri.net
   -->
   
   <!-- Footer 1 - Left Social/Right Menu -->
   <footer>
    <div class="container">
      <div class="row text-center">
      
                <div class="col-md-6 col-sm-6 col-xs-12">
                 <ul class="list-inline">
                            
                              <li>
                                   <a href="#"><i class="fa fa-facebook fa-2x"></i></a>
                              </li>
                              
                              <li>
                                   <a href="#"><i class="fa fa-dropbox fa-2x"></i></a>
                              </li>
                              
                              <li>
                                   <a href="#"><i class="fa fa-flickr fa-2x"></i></a> 
                              </li>
                             
                              <li>
                                   <a href="#"><i class="fa fa-github fa-2x"></i></a>
                              </li>
                               
                              <li>
                                   <a href="#"><i class="fa fa-linkedin fa-2x"></i></a>
                              </li>
                              
                              <li>
                                   <a href="#"><i class="fa fa-tumblr fa-2x"></i></a>
                              </li>
                               
                              <li>
                                   <a href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                             </li>
                             
                    </ul>
                  </div>
          
                <div class="col-md-6 col-sm-6 col-xs-12">
                 <ul class="menu list-inline">
                            
                        <li>
                           <a href="#">Home</a>
                         </li>
                              
                         <li>
                            <a href="#">About</a>
                         </li>
                              
                         <li>
                           <a href="#">Blog</a>
                         </li>
                              
                         <li>
                            <a href="#">Gallery </a>
                         </li>
                              
                         <li>
                           <a href="#">Contact</a>
                        </li>
              
                </ul>
              </div>
          
          
          </div> 
       </div>
   </footer>
   
   <div class="copyright">
    <div class="container">
      
        <div class="row text-center">
            <p>Copyright - NVK © 2018 All rights reserved</p>
        </div>
        
       </div>
   </div>
   <!-- End -->
   
   

    <!-- end Footer -->
    <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="public/js/my.js"></script>
    <script > 
        $(document).ready(function(){
            CKEDITOR.replace('noidung');
        });
    </script>
</body>

</html>