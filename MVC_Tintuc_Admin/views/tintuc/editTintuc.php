<?php
$tintuc = $data['data'];
$pathImg = '../tintuc/'.$tintuc['Hinh'];

?>
<div class="row carousel-holder">
    		<div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default">               
				  	<div class="panel-heading"><?=$data['title']?></div>
				  	<div class="panel-body">
                        <form method="POST" action="?c=tintuc&a=postEditTintuc" enctype="multipart/form-data" >
                            
                            <input type="hidden" name="id" id="input" class="form-control" value="<?=$tintuc['id']?>">
                            
                            <div class="form-group">
                                <input type="text" class="form-control" name="tieude"  value="<?=$tintuc['TieuDe']?>" placeholder="Tiêu đề">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="tieudekhongdau" value="<?=$tintuc['TieuDeKhongDau']?>" placeholder="Tiêu đề không dấu">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="tomtat" value="<?=$tintuc['TomTat']?>" placeholder="Tóm tắt">
                            </div>
                            <div class="form-group">
                                <textarea name="noidung" id="input" class="form-control"  rows="3" required="required"><?=$tintuc['NoiDung']?></textarea>       
                            </div>
                            <div class="form-group">     
                                
                                <input type="hidden" name="HinhHienTai" id="input" class="form-control" value="<?=$tintuc['Hinh']?>">
                                 
                                <input type="file" value="<?=$pathImg?>" name="file" class="form-control">
                                <img src="<?=$pathImg?>" class="img-responsive" alt="Image">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="noibat" value="1" >
                                    Nổi bật
                                </label>
                            </div>
                          
                            <div class="form-group">
                                
                                <select name="loaitin" id="input" class="form-control">
                                    <?php 
                                    foreach($data['loaitin'] as $loaitin){
                                    ?>
                                    <option value=<?=$loaitin['id']?>><?=$loaitin['Ten']?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                                
                            </div>
                            <input type="submit" name="btnEditTintuc" class="btn btn-primary" value="Sửa"></input>
                        </form>  
				  	</div>
				</div>
            </div>
            <div class="col-md-3"></div>
</div>