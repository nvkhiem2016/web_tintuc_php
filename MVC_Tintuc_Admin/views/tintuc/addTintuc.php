
<div class="row carousel-holder">
    		<div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default">               
                    <?php
                        if(isset($data['message'])){
                            echo "<div class='alert alert-danger'>".$data['message']."</div>";
                        }
                    ?>
				  	<div class="panel-heading">Thêm tin tức</div>
				  	<div class="panel-body">
                        <form method="POST" action="?c=tintuc&a=postAddTintuc" enctype="multipart/form-data" >
                            <div class="form-group">
                                <input type="text" class="form-control" name="tieude"  placeholder="Tiêu đề">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="tieudekhongdau" placeholder="Tiêu đề không dấu">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="tomtat" placeholder="Tóm tắt">
                            </div>
                            <div class="form-group">
                            
                                <textarea name="noidung" id="noidung" rows="10" cols="80"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="file" name="file" class="form-control">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="noibat" value="1" >
                                    Nổi bật
                                </label>
                            </div>
                            <div class="form-group">
                                
                                <select name="loaitin" id="input" class="form-control">
                                    <option value="-1">-- Select One --</option>
                                    <?php 
                                    foreach($data['loaitin'] as $loaitin){
                    
                                    ?>
                                    <option value=<?=$loaitin['id']?>><?=$loaitin['Ten']?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                                
                            </div>
                            <input type="submit" name="btnAddTintuc" class="btn btn-primary" value="Thêm"></input>
                        </form>  
				  	</div>
				</div>
            </div>
            <div class="col-md-3"></div>
</div>