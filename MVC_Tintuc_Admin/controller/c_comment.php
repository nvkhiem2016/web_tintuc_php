<?php
	session_start(); 
	if(isset($_COOKIE['admin']) && !empty($_COOKIE['admin']) && isset($_SESSION['name'])){
		
	}else{
		header("location:index.php?c=user&a=getDangnhap");
	}
	class c_comment extends NVK_Controller
	{
        public $comment;
		function __construct()
		{
			parent::__construct();
            //$this-> load->helper('');//load helper với tên : ...
            //load model
            $this->load->model('m_comment','');

            //khởi tạo đối tượng
            $this-> comment = new m_comment();
		}
		public function html($data1,$path,$data2,$data3)
		{
			//require_once('views/v_home.php');
			$this-> load-> view('header','');
			$this-> load-> view('comment/'.$path,$data2);
			$this-> load-> view('footer','');
        }
        public function getAllComment()
        {
            $id_tin = isset($_GET['id'])?$_GET['id']:'';
            $path = 'index';
            $data['title']="";
            $data['data'] = $this->comment->getAllCommentById($id_tin);
             
            return $this->html($data,$path,$data,'');
        }
        public function deleteComment()
        {
            $id = isset($_GET['id_comment'])?$_GET['id_comment']:'';
            $delete = $this->comment->deleteComment($id);
            if($delete)
            {
                header("location:".$_SERVER['HTTP_REFERER']);
            }
        }
		
	}
?>