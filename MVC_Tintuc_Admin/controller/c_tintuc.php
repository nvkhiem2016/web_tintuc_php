<?php 
	session_start(); 
	if(isset($_COOKIE['admin']) && !empty($_COOKIE['admin']) && isset($_SESSION['name'])){
		
	}else{
		header("location:index.php?c=user&a=getDangnhap");
	}
	class c_tintuc extends NVK_Controller
	{
		public $tintuc;
		public $loaitin;	
		function __construct()
		{
			parent::__construct();
            //$this-> load->helper('');//load helper với tên : ...
			$this-> load->model('m_tintuc','');
			$this-> load->model('m_loaitin','');
			//require_once('library/pagination_library.php');
			//load library
			$this-> load->library('pagination');
			//load helper
			$this-> load->helper('uploads');
			$this-> load->helper('delete_img');
			//load model
			$this-> tintuc = new m_tintuc();
			$this-> loaitin = new m_loaitin();
        }
        public function html($data1,$path,$data2,$data3)
		{
			$this-> load-> view('header',$data1);
			$this-> load-> view('tintuc/'.$path,$data2);
			$this-> load-> view('footer',$data3);
		}
		public function index()
		{
			$data['title'] = "Trang quản lý tin tức";
			
            $this->getAllTintuc($data);
			
        }
        public function deleteTintuc()
		{
			$id = $_GET['id']?$_GET['id']:'';
			$page=$_GET['page'];
            $result = $this->tintuc->deleteTintuc($id);
            if($result){
				header("location:".$_SERVER['HTTP_REFERER']);
            }else {
				echo "<h1>Xoá không thành công</h1>";
			}
		}
		public function getEditTintuc()
		{
			$id = isset($_GET['id'])?$_GET['id']:'';
			$data['title'] = "Sửa tin tức";
			
			$data['data'] = $this->tintuc->getTintucbyId($id);
		
			$data['loaitin']=$this->loaitin->getAllLoaitin();
			$path = "editTintuc";
			$this->html($data,$path,$data,'','');
		}
		public function postEditTintuc()
		{
			if(isset($_POST['btnEditTintuc'])){
				$data=array();
				$data['id'] = (int)$_POST['id'];
				$data['TieuDe'] = $_POST['tieude'];
				$data['TieuDeKhongDau'] = $_POST['tieudekhongdau'];
				$data['TomTat'] = $_POST['tomtat'];
				$data['NoiBat'] = isset($_POST['noibat'])?1:0;	
				$data['idLoaiTin'] = (int)$_POST['loaitin'];
				$data['NoiDung'] = $_POST['noidung'];
				if($_FILES["file"]["name"]){//kiểm tra ng dùng có chọn file chưa
					$data['Hinh'] = uploadImg();//hàm trong uploads_helper
					if(isset($data['Hinh']) && $data['Hinh']!==$_POST['HinhHienTai']){ // kiểm tra giá trị trả về
						deleteImg($_POST['HinhHienTai']);
					}
				}else{
					$data['Hinh'] = $_POST['HinhHienTai'];
				}
				
				$update = $this->tintuc->updateTintuc($data);
				if(!$update){
					die("Lỗi!!! Không sửa được tin tin ...");
				}else{
					header('Location:index.php?c=tintuc');
				}
				
			}
		}
		public function getAddTintuc()//hiển thị form thêm tin tức
		{
			$data['title'] = "Thêm tin tức";
			$path = "addTintuc";
			$data['loaitin']=$this->loaitin->getAllLoaitin();
			$this->html($data,$path,$data,'','');
		}
		public function postAddTintuc()//thêm tin tức
		{
			if(isset($_POST['btnAddTintuc'])){
				$data=array();
				$data['TieuDe'] = $_POST['tieude'];
				$data['TomTat'] = $_POST['tomtat'];
				$data['NoiBat'] = isset($_POST['noibat'])?1:0;
				$data['TieuDeKhongDau'] = $_POST['tieudekhongdau'];
				$data['idLoaiTin'] = (int)$_POST['loaitin'];
				$data['NoiDung'] = $_POST['noidung'];
				$data['Hinh'] = uploadImg();//hàm trong uploads_helper
				$insert = $this->tintuc->insertTintuc($data);
				if($insert){	
					$data['message'] = "Đã thêm tức tức thành công";
					return $this->html('','addTintuc',$data,'');
				}else{
					die("Lỗi!!! Không thêm được tin tin ...");		
				}
			}
		}
        public function getAllTintuc($title)
		{
			$data = array();
			
			$data['loaitin']=$this->loaitin->getAllLoaitin();
			$data['data'] = $this-> tintuc-> getAllTintuc();
			$path = 'index';
			$page=isset($_GET['page'])?$_GET['page']:1;//lấy page hiện tại
			$pagination = new pagination(count($data['data']),$page);//phân trang
			$limit = $pagination->_nItemOnPage;
			$vitri = ($page)*$limit;
			$data['data']=$this-> tintuc-> getAllTintuc($vitri,$limit);
			$data['pagination'] = $pagination->showPagination();
			$this-> html($title,$path,$data,'','');
		}
		public function filterTintucbyLoaitin() //lọc tin tức theo loại tin
		{
			$data = array();
			$id = isset($_GET['idLoaitin'])?$_GET['idLoaitin']:'';
			$data['loaitin']=$this->loaitin->getAllLoaitin();
			$data['data'] = $this-> tintuc-> getAllTintucbyLoaitin($id);
			$path = 'filter';
			$page=isset($_GET['page'])?$_GET['page']:1;//lấy page hiện tại
			$pagination = new pagination(count($data['data']),$page,1,5);//phân trang
			$limit = $pagination->_nItemOnPage;
			$vitri = ($page)*$limit;
			$data['data']=$this-> tintuc-> getAllTintucbyLoaitin($id,$vitri,$limit);
			$data['pagination'] = $pagination->showPagination();
			$this-> html('',$path,$data,'','');
		}
		public function searchTintuc()
		{
		
				$key = $_GET['keySearch'];
				$data['title']="Trang tìm kiếm";
				$data['loaitin']=$this->loaitin->getAllLoaitin();
				$data['data'] = $this->tintuc->searchTintuc($key);
				$this->html($data,'timkiem',$data,'');

		}
	}
?>