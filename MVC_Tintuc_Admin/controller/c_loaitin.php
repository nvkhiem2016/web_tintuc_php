<?php 

	session_start(); 
	if(isset($_COOKIE['admin']) && !empty($_COOKIE['admin']) && isset($_SESSION['name'])){
		
	}else{
		header("location:index.php?c=user&a=getDangnhap");
	}
	class c_loaitin extends NVK_Controller
	{
		public $tintuc;
		public $loaitin;
		function __construct()
		{
			parent::__construct();
            //$this-> load->helper('');//load helper với tên : ...
			//$this-> load->model('m_tintuc','');
			$this-> load->model('m_loaitin','');
			//require_once('library/pagination_library.php');
			$this-> load->library('pagination');
			$this-> load->helper('uploads');
			//$this-> tintuc = new m_tintuc();
			$this-> loaitin = new m_loaitin();
        }
        public function html($data1,$path,$data2,$data3)
		{
			$this-> load-> view('header',$data1);
			$this-> load-> view('loaitin/'.$path,$data2);
			$this-> load-> view('footer',$data3);
        }
        public function index()
		{
			$data['title'] = "Trang quản lý loai tin";
			$this->getAllLoaitin($data);
		}
		public function getAddLoaitin()//hiển thị form thêm tin tức
		{
			$data['title'] = "Thêm Loại Tin";
			$path = "addLoaitin";
			$data['theloai']=$this->loaitin->getAllTheloai();
			$this->html($data,$path,$data,'','');
		}
		public function postAddLoaitin()
		{
			if(isset($_POST['btnAddLoaitin'])){
				$data=array();
				$data['Ten'] = $_POST['ten'];
				$data['TenKhongDau'] = $_POST['tenkhongdau'];
				$data['idTheLoai'] = (int)$_POST['theloai'];
				$insert = $this->loaitin->insertLoaitin($data);
				if(!$insert){
					die("Lỗi!!! Không thêm được tin tin ...");
				}else{
					header("Location:index.php?c=loaitin");
				}
				
			}
		}
		public function getEditLoaitin()
		{
			$id = isset($_GET['id'])?$_GET['id']:'';
			
			$data['title'] = "Sửa loại tin";
			$data['data'] = $this->loaitin->getLoaitinByid($id);
			$data['theloai']=$this->loaitin->getAllTheloai();
			$path = "editLoaitin";
			$this->html($data,$path,$data,'','');
		}
		public function postEditLoaitin()
		{
			if(isset($_POST['btnEditLoaitin'])){
				$data=array();
				$data['id']=$_POST['id'];
				$data['Ten'] = $_POST['ten'];
				$data['TenKhongDau'] = $_POST['tenkhongdau'];
				$data['idTheLoai'] = (int)$_POST['theloai'];
				$update = $this->loaitin->updateLoaitin($data);
				if(!$update){
					die("Lỗi!!! Không thêm được tin tin ...");
				}else{
					header("Location:index.php?c=loaitin");
				}	
			}
		}

		public function getAllLoaitin($title)
		{
			$data = array();
			$data['data'] = $this-> loaitin-> getAllLoaitin();
			$path = 'index';
			$page=isset($_GET['page'])?$_GET['page']:1;//lấy page hiện tại
			$pagination = new pagination(count($data['data']),$page);//phân trang
			$limit = $pagination->_nItemOnPage;
			$vitri = ($page)*$limit;
			$data['data']=$this-> loaitin-> getAllLoaitin($vitri,$limit);
			$data['pagination'] = $pagination->showPagination();

			$this-> html($title,$path,$data,'','');
		}
		public function deleteLoaitin(){
			$id = $_GET['id']?$_GET['id']:'';
			$page=$_GET['page'];
			echo $page;
            $result = $this->loaitin->deleteLoaitin($id);
            if($result){
                header("location:".$_SERVER['HTTP_REFERER']);
            }else {
				echo "<h1>Xoá không thành công</h1>";
			}
		}
	}
?>