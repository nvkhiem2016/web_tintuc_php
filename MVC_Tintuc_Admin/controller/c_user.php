<?php 
    
	class c_user extends NVK_Controller
	{
        public $user;
		function __construct()
		{
			parent::__construct();
			$this-> load->model('m_user','');
			//load library
			$this-> load->library('');
			//load helper
			$this-> load->helper('');
			$this-> load->helper('');
			//load model

			$this-> user = new m_user();
		}
        public function index()
		{
			$data['title'] = "Trang quản lý thành viên";
			$path ='index';
            $this->html($data,$path,'','');
			
        }
        public function html($data1,$path,$data2,$data3)
		{
			$this-> load-> view('header',$data1);
			$this-> load-> view('users/'.$path,$data2);
			$this-> load-> view('footer',$data3);
		}
        public function getDangnhap()
        {
            $data['title'] = "Trang Đăng Nhập";
            $path = 'dangnhap';
            $this->html($data['title'],$path,'','');
        }
        public function postDangnhap()
        {
            if(isset($_POST['btnDangnhap'])){
                $email = isset($_POST['email'])?$_POST['email']:'';
                $password =isset($_POST['password'])?$_POST['password']:'';
                $password = md5($password);//mã hoá mật khẩu
                $data['data'] = $this->user->loginUser($email,$password);
                if($data['data']){
                    $id = $data['data']['id'];
                    setcookie('admin',$email,time() + (86400 * 30));
                    setcookie('id',$id,time() + (86400 * 30));                  
                    session_start();
                    $_SESSION['name'] = $data['data']['name'];
                    unset($_SESSION['chua_dang_nhap']);
                    header('location:index.php');
                }else{
                    $data['message'] = "Tài khoản không tồn tại, hoặc mật khẩu không chính xác, hoặc không phải admin";
                    //echo "<script type='text/javascript'>alert('{$data['message']}');</script>";
                    $this->html('','dangnhap',$data ,'');
                }
            }
        } 
        public function getDoimatkhau(){
            $path = 'doimatkhau';
            $data['title'] = "Đổi mật khẩu";
            $this->html($data['title'],$path,'','');
        }
        public function postDoimatkhau(){
            if(isset($_POST['btnDoimatkhau']))
            {
                $data['oldpassword'] = md5($_POST['Oldpassword']);
                $data['password'] = md5($_POST['Newpassword']);
                $data['email'] =  $_COOKIE['admin'];
                $data['id'] = $_COOKIE['id'];
                $user = $this->user->getUserbyId($data['id']);
                if($data['oldpassword']!==$user['password']){
                    $data['message'] = "Mật khẩu cũ không chính xác";
                    //echo "<script type='text/javascript'>alert('{$data['message']}');</script>";
                    $this->html('','doimatkhau',$data ,'');
                }else{
                    $update = $this->user->doimatkhau($data);
                    if($update)
                    {      
                        $data['message'] = "Bạn đã đổi mật khẩu thành công";
                        //echo "<script type='text/javascript'>alert('{$data['message']}');</script>";
                        $this->html('','doimatkhau',$data ,'');
                        
                    }
                }
            }
        }
        public function dangxuat()
        {
            $email =  $_COOKIE['admin'];
            session_start();
            session_destroy();
            setcookie('admin',$email,time()-1);//set time về 0
            header('location:index.php?c=home');
        }
	}
?>